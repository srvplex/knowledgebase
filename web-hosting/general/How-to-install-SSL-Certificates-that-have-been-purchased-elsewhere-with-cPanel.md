
#How to install SSL Certificates that have been purchased elsewhere with cPanel

The SSL/TLS Manager will allow you to generate SSL certificates, certificate signing requests, and private keys. These are all parts of using SSL to secure your website. SSL allows you to secure pages on your site so that information such as logins, credit card numbers, etc are sent encrypted instead of plain text. It is important to secure your site’s login areas, shopping areas, and other pages where sensitive information could be sent over the web.

_(cPanel >> Home >> Security >> SSL/TLS)_

##Overview

The features in this interface allow you to generate and manage SSL certificates, signing requests, and keys, which enhance your website’s security. They are useful for websites that regularly work with sensitive information, such as login credentials and credit card numbers. Encryption protects visitors’ communications from malicious users.

Warning:

As of cPanel & WHM version 68, we only support Transport Layer Security (TLS) protocol [version 1.2](https://tools.ietf.org/html/rfc5246).

We will only support applications that use [TLSv1.2](https://tools.ietf.org/html/rfc5246).
We strongly recommend that your hosting provider enable [TLSv1.2](https://tools.ietf.org/html/rfc5246) for your account.

Note:

CAA records in the domain’s zone file restrict which CAs (Certificate Authority) may issue certificates for that domain.

If no CAA records exist for a domain, all CAs can issue certificates for that domain.
If conflicting CAA records already exist, remove the existing CAA records or add one for the desired CA.

For example, a CAA record for Comodo would resemble the following example, where `example.com` represents the domain name:

You can manage CAA records through the [Zone Editor](https://documentation.cpanel.net/display/68Docs/Zone+Editor) interface. For more information about a CA’s requirements, read their documentation.

##Documents available

The following documents provide more information about the sections of this interface:

[Private Keys](https://documentation.cpanel.net/display/68Docs/Private+Keys+-+KEY) – KEY — Generate, view, upload, or delete private keys.
[Certificate Signing Requests](https://documentation.cpanel.net/display/68Docs/Certificate+Signing+Requests+-+CSR) – CSR — Generate, view, or delete SSL certificate signing requests.
[Certificates](https://documentation.cpanel.net/display/68Docs/Certificates+-+CRT) – CRT— Generate, view, upload, or delete SSL certificates.
[Install and Manage SSL for your site HTTPS](https://documentation.cpanel.net/display/68Docs/Install+and+Manage+SSL+for+your+site+HTTPS) — Set up an SSL certificate for the site.
[Manage Certificate Sharing](https://documentation.cpanel.net/display/68Docs/Manage+Certificate+Sharing) — Change your site’s certificate sharing permissions.

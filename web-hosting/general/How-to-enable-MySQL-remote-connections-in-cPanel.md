
#How to enable MySQL remote connections in cPanel

##Enabling Remote MySQL in the Domain cPanel Interface

Log in to the domain’s cPanel interface and find the section on the main page labelled Databases.

In the Databases section find the link/button labelled Remote MySQL and click on it.

Add a hostname or IP address that you want to grant remote MySQL access to and then click the Save button

If a host or IP address needs to be removed from this list you can click the ‘Delete’ button next to the entry in the list.

Once you have made your changes, additions, or removals to the list you can return the main page of the cPanel interface, or log out if you have no other tasks to take care of.

|| Important: We reserve the right to block all inbound MySQL connections in order to prevent attacks to the mysqld service. If you are unable to connect, please contact support so that we can add your IP address to our whitelist.

#I am getting an error on my site. Where can I find my error logs / error_log files?

##Error logging when using our ‘native’ PHP configuration:

If you are experiencing a 500 error on your website, in most cases this is due to a PHP error / coding error.

To diagnose PHP errors on your site, you can check (either via the File Manager in cPanel, or via FTP / SSH) for files named ‘error_log’ The location of the file will generally be where the code is executed – for example, if you have experienced a 500 error on the homepage / index of your site, the error_log file will be located within the public_html directory.

If, however, you are only experiencing the 500 error in a particular directory (such as wp-admin as an example) then the error_log file will be located in the public_html/wp-admin directory.

##Error logging when you are using ‘nginx’ via CloudNS or Bolt-Cache:

If you are using our CLOUDNS or Bolt-Cache plugins and have switched to Native NGINX, then your PHP will be processed using PHP-FPM.

All errors will be logged to the following directory… /home/accountname/logs/ Specifically, the PHP error logs will be… /home/accountname/logs/php_error_log

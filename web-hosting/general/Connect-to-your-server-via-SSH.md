#Connect to your server via SSH

SSH allows secure file transfer and remote logins over the internet. Your connection via SSH is encrypted allowing the secure connection. In this section you can manage your SSH keys to allow automation when logging in via SSH. Using public key authentication is an alternative to password authentication. Since the private key must be held to authenticate, it is virtually impossible to brute force. You can import existing keys, generate new keys, as well as manage/delete keys.

SSH (Secure Shell) is a program to log into another computer/server over a network securely. It provides strong authentication and secure communications over insecure channels. Your login, commands, text are all encrypted when using ssh.

_(cPanel >> Home >> Security >> SSH Access)_

#Overview

This interface provides information about how to connect to another web server via the SSH (secure shell) network protocol. The SSH (secure shell) network protocol allows you to connect to another web server over the Internet via a command line interface (CLI). You can use this network protocol to remotely manage your server, configure CGI scripts, and perform other tasks. Many modern operating systems, such as MacOS® and Linux distributions, include SSH. If you use Microsoft Windows® to connect to your server, you must use an SSH client, such as [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/), to log in to your server. Many Unix-based operating systems include standardized commands. For a list of standardized Unix-based (POSIX) commands, read the One-Serve website documentation.

Note:

Whilst we do provide SSH access, you will need to ensure that it is enabled in your package. If you suspect that you don’t have SSH access, but require it, then you can click ‘Open Ticket’ from our client area and let the support team know you would like SSH access enabling.
Connect to your server via SSH

The following sections describe how to connect to your server via various SSH clients. To in to a server via SSH with PuTTY and a public key, perform the following steps:

    From the Windows _Start _menu, open the client.
    Navigate to the ***PuTTY Key Generator*** interface.
    Under the Actions heading, click Generate. PuTTY generates the key and displays the result under the Key menu.
    Copy the public key and paste it in the `.ssh/authorized_keys` file.
    Enter a passphrase in the Key passphrase _and _Confirm passphrase text boxes.
    Click Save private key and save the key as a `.ppk` file. Important: You **must** save PuTTY keys as `.ppk` files.
    In the Session _interface, from the _Saved Sessions menu, select your preferred authorization session and click Load.
    Navigate to the Auth interface under the SSH category.
    Click Browse, select the private key file to upload, and click Open.
    Navigate to cPanel’s_ Manage SSH Keys interface ( Home >> Security >> SSH Access >> Manage SSH Keys) _and import the server’s keys.

#Manage SSH keys

This section of cPanel’s _SSH Access_ interface allows you to create, import, manage, and remove SSH keys. The system will use these keys when you confirm that a specific computer has the right to access your website’s information with SSH.

##Generate a New Key

Use this section of the interface to create new SSH key pairs, which include a public key and a private key. To generate a new SSH key pair, perform the following steps:

    Click ***Generate a New Key***.
    To use a custom key name, enter the key name in the Key Name (_This value defaults to id_rsa_): text box. Note: If you use a custom key name, you must manually specify the SSH key when you log in to the server.
    Enter and confirm the new password in the appropriate text boxes. Notes:
        The system evaluates the password that you enter on a scale of 100 points. 0 indicates a weak password, while 100 indicates a very secure password.
        Some web hosts require a minimum password strength. A green password Strength meter indicates that the password is equal to or greater than the required password strength.
        Click Password Generator to generate a strong password. For more information, read our [Password & Security](https://documentation.cpanel.net/display/68Docs/Password+and+Security) documentation.
    Select the desired key type.
        DSA keys provide quicker key generation and signing times.
        RSA keys provide quicker verification times.
    Select the desired key size. Note: Greater key sizes are more secure, but they result in larger file sizes and slower authentication times.
    Click Generate Key. The interface will display the saved location of the key. Important: For the new SSH key to function, you **must** authorize the SSH key. For more information, read the [Manage your keys](https://documentation.cpanel.net/display/68Docs/SSH+Access#SSHAccess-ManageExisting) section.

###Import Key

To import an existing SSH key, perform the following steps:

    Click Import Key.
    To use a custom key name, enter the key name in the Choose a name for this key (_defaults to id_rsa_) text box. Important: If you use a custom key name, you must manually specify the SSH key when you log in to the server.
    Paste the public and private keys into the appropriate text boxes.
    Click Import.


#How do I change PHP versions, or change my PHP settings / config?

Here at SRVPlex, we have quite a unique configuration in terms of the way PHP is handled. By default, we use ‘nginx’ which reverse proxies to Apache, then the PHP itself is handled by a handler called ‘mod_lsapi‘.

###Default PHP Version

The default version of PHP on our servers is PHP 7.2.

If you haven’t used our ‘CLOUDNS’ plugin in cPanel to use full-nginx, then you will be using our default configuration as mentioned above – this means that you would use the ‘Select PHP Version’ plugin in cPanel to modify the version of PHP you are running. You would also use the same plugin for configuring the modules that are enabled for your scripts.

##How to use the ‘Select PHP Version’ option in cPanel

[Here](https://youtu.be/HW9mgfqhjSQ) is a quick video tutorial on how to change your PHP version using cPanel…

Users can find PHP Selector functionality under the “Select PHP version” in their cPanel home page.

On the PHP Selector page an you will see the currently selected PHP version – for new accounts, it will be the default version for their account.

They can choose the desired version from those allowed by the administrator by clicking on the drop-down list.

After choosing the PHP version, users can select modules they want to be available for their website. Let’s select gmagick and rar for this example.

Clicking on ‘Save’ button makes the modules immediately available for the web application.

On the top right, there is a link to PHP options where end-users can adjust the most important values. Let’s change memory limit to 128Mb and post_max_size to 32Mb.

End users do not need to modify any files like “php.ini” or “htaccess” manually – everything is done from the web interface.

##How to change PHP version when you are using ‘CloudNS’ or Bolt-Cache (nginx)

If you want to benefit from our PHP-FPM / Nginx stack (for enhanced performance) then the above will not change your PHP version, unless your site is set to ‘Proxy’ (which is the default for a new account). If your account is using ‘PHP’ from the CLOUDNS plugin, then you are using ‘PHP-FPM’ and you are using nginx instead of apache. To change PHP versions when running pure nginx (or, PHP-FPM / nginx)…

    1. Login to cPanel
    2. Go to the ‘CLOUDNS’ plugin
    3. Select the domain you wish to change from the dropdown menu
    4. Click ‘Configure’
    5. Select ‘PHP’ from the dropdown list under the ‘Application Server’ section
    6. Hit ‘Submit’
    7. Select the relevant PHP version from the ‘Backend Version’ dropdown list
    8. Select a ‘Template’ then hit ‘Submit’


#I am getting an intermittent 500 error on my site

If the 500 error is intermittent, then it’s likely your account is hitting the memory_limit for the account.

To increase this, see the following…

[How do I change PHP versions, or change my PHP settings / config?](https://srvplex.com/clients/knowledgebase/5/How-do-I-change-PHP-versions-or-change-my-PHP-settings-or-config.html)

Then set memory_limit = 512M

If that doesn’t help, then we would recommend checking your PHP error logs…

[I am getting an error on my site. Where can I find my error logs / error_log files?](https://srvplex.com/clients/knowledgebase/6/I-am-getting-an-error-on-my-site.-Where-can-I-find-my-error-logs-or-error_log-files.html)

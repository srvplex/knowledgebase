#Monitor visitors that are logged into your site through FTP using cPanel

_(cPanel >> Home >> Files >> FTP Connections)_

##Overview

This interface displays information about current connections to your FTP server and it allows you to terminate those connections.

##View current FTP sessions

The interface includes the following information about connections to your FTP server:

To refresh the list of FTP sessions, click Reload.

##Disconnect users from an FTP session

If you suspect malicious activity from a user, or if the user is idle, you can disconnect their FTP session. To disconnect a user from an FTP session, perform the following steps:

    1. Click Disconnect next to the FTP session that you wish to disconnect.
    2. To verify that the server disconnected the session, click Reload.


#How to force your site to use SSL (https) using cPanel

| First of all, you will need to install an SSL certificate to your site – a guide on how to do this can be found [here](https://srvplex.com/clients/knowledgebase/2/Install-a-free-SSL-via-cPanel-or-AutoSSL.html).

Once your certificate is installed, you will need to redirect from ‘http://’ to ‘https://’ – this can be achieved in a number of ways…

##Using our CloudNS plugin

From within cPanel, go to…

    1. Nginx Cluster Control -> CLOUDNS
    2. Next, select the domain drop the dropdown list and click ‘Configure’
    3. Click ‘APPLICATION SETTINGS’
    4. Scroll down until you see a section for ‘Redirections’
    5. ‘Enable’ the redirect_to_ssl option
    6. Click ‘Submit’

That’s it! You are done – now, your site will be forcing the https:// protocol instead of using http://

##Force https:// via a .htaccess file

To force all web traffic to use HTTPS insert the following lines of code in the .htaccess file in your website’s root folder. Important:If you have existing code in your .htacess, add this above where there are already rules with a similar starting prefix.

`RewriteEngine On RewriteCond %{SERVER_PORT} 80 RewriteRule ^(.*)$ https://www.example.com/$1 [R,L]`

Be sure to replace www.example.com with your actual domain name. To force a specific domain to use HTTPS, use the following lines of code in the .htaccess file in your website’s root folder:

`RewriteEngine On RewriteCond %{HTTP_HOST} ^example\.com [NC] RewriteCond %{SERVER_PORT} 80 RewriteRule ^(.*)$ https://www.example.com/$1 [R,L]`

Make sure to replace example.com with the domain name you’re trying force to https. Additionally, you need to replace www.example.com with your actual domain name. If you want to force SSL on a specific folder you can insert the code below into a .htaccess file placed in that specific folder:

`RewriteEngine On RewriteCond %{SERVER_PORT} 80 RewriteCond %{REQUEST_URI} folder RewriteRule ^(.*)$ https://www.example.com/folder/$1 [R,L]`

Make sure you change the folder reference to the actual folder name. Then be sure to replace www.example.com/folderwith your actual domain name and folder you want to force the SSL on.

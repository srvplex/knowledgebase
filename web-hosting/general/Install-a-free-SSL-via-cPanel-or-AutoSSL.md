#Install a free SSL via cPanel / AutoSSL

Installing an SSL certificate in cPanel is easy! Best of all, its free!

|| Important: Installing a certificate does will not force your browsers to visit the site via https:// – to do this, you would need to see our ‘How to force your site to use SSL (https) using cPanel’ guide.

We are pleased to announce we have now rolled out some significant changes to the way in which SSL certificates are handled.

For many years, we have used the ‘FleetSSL’ plugin for installing SSL certificates, however, due to some recent cPanel upgrades/changes and a lack of updates from the software vendors at FleetSSL, we have seen a number of clients raise issues with failed renewals.

The issue was caused by some incompatibilities between the solutions, which has led us to completely remove the previously used FleetSSL plugin from cPanel.

We have now enabled the recommended solution developed by cPanel, which is the ‘AutoSSL’ functionality using the Sectigo SSL’s as opposed to LetsEncrypt. There are a number of reasons for the change, however what we can assure you is that both LetsEncrypt and the Sectigo SSL’s are identical in all ways, and offer identical levels of protection. The core reason for the change is that LetsEncrypt imposes fairly strict Rate Limits, preventing / failing some installations from completing before expiry when used on larger servers.

The process of installing SSL’s has been simplified, in most cases requiring no interaction on your part. The purpose of AutoSSL, is that we will as a provider automatically check for SSL’s which are expiring, and will automatically replace those SSL’s, ensuring your sites remain secure.

Once you enable AutoSSL, your websites are automatically secured with a free, Domain Validated SSL certificate. Perhaps more exciting is the fact that your coverage will never lapse, because at expiration time a new, free SSL is requested and automatically installed.

In terms of the change, you aren’t required to do anything. We have taken care of implementation on all accounts/servers for you.

If you are a reseller and wish to disable AutoSSL for a particular client, you can do so via the ‘Feature Manager’ in WHM.

Please note that existing LetsEncrypt certificates will remain in-tact, until the SSL’s are due to expire. 3 days before expiry, the new SSL certificates will be installed automatically.

#Install a new Certificate

If you want to install the new SSL immediately, then you can do so by going to cPanel -> SSL / TLS and uninstalling your existing certificates.

Following this, you can then run AutoSSL manually for the account by going to cPanel -> SSL / TLS Status, and clicking the ‘Run AutoSSL‘ option for the account.

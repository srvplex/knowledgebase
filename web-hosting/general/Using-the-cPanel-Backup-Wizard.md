
#Using the cPanel Backup Wizard

##Overview

The Backup Wizard interface allows you to back up all or part of your website, or to restore your website from the most recent backup file.
Warning

As a SRVPlex customer, you will also have access to our full account backup facility powered by JetBackups – this is the preferred method for restoring files or databases on your account

##Back Up

You can choose to either create a full backup file or a partial backup file.

Important:

    * You cannot use a full backup file to restore your files through the cPanel interface.
    * You must download and compress the backup file prior to restoration.
    * After you decompress the backup file, you can upload and restore individual portions of your website that the full backup file contained.

###Create a full backup

To create a full backup file, perform the following steps:

    1. From the main Backup Wizard interface, click Back Up. The Full or Partial Backup section of the interface will appear.
    2. Click Full Backup. The Download section of the interface will appear.
    3. Select one of the following destinations from the Backup Destination menu:
        * Home Directory — Select this option to save the backup file to the server.
        * Remote FTP Server — Select this option to use FTP to store the backup file on a remote server.
        * Remote FTP Server (passive mode transfer) — Select this option to use passive FTP to store the backup file on a remote server.
        * Secure Copy (SCP) — Select this option to use SCP to store the backup file on a remote server.
    4. Perform one of the following actions to configure notifications for this backup file:
        * To receive a notification when the backup process finishes, enter your email address in the Email Address text box.
        * To disable notifications, select the Do not send email notification of backup completion. checkbox.
    5. If you selected the Remote FTP Server, Remote FTP Server (passive mode tansfer), or Secure Copy (SCP) destinations, enter the remote destination’s information in the available text boxes.
    6. Click Generate Backup. A confirmation message will appear. Click Go Back to return to the Download section of the interface.

Note:

If you selected the Home Directory, _click the filename in the _Download section of the interface to download the backup file.

    The backup file’s creation date appears in the filename (which begins with `backup-MM-DD-YYYY`).
    The system stores full backup files as tarballs that use the `.tar.gz` file extension.

###Create a partial backup

To back up a portion of your site, perform the following steps:

    From the main Backup Wizard interface, click Back Up. The Full or Partial Backup section of the interface will appear.
    Click the portion of your site that you wish to back up:
        Home Directory — Back up the `/home` directory.
        MySQL Databases — Back up your databases.
        Email Forwarders & Filters — Back up your email forwarders or filters.
    The Download section of the interface will appear.
    Click the desired backup file to download it.

Note

When you create a partial backup, the backup file’s type and extension depend on the backup type.

###Restore

To restore a portion of your site from an existing backup, perform the following steps:

    Click Restore. The Select Restore Type section of the interface will appear.
    Click the portion of your site that you wish to restore:
        Home Directory — Restore the `/home` directory.
        MySQL Databases — Restore your databases.
        Email Forwarders & Filters — Restore your email forwarders or filters.
    The Restore section of the interface will appear.
    Click Choose File and select the desired backup file. Remember: The type of backup file that the system uses depends on the portion of your site that you back up.
    Click Upload to begin the restoration process.

##Backup file contents

When you back up your home directory, the backup file includes the following files:

    All of the files that you own.
    Files that you do not own, but to which you have access.

Note:

Backup files do not include files from your `/home` directory that you do not own and cannot access.

To exclude certain files and directories from a backup file, place a configuration file in your home directory. For more information, read our How to Exclude Files From Backups documentation.

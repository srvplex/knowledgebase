
#503 Error – Resource Limit Reached

A 503 error suggests the account is hitting it’s LVE limits (Resource Limits Reached)

These limits are designed to provide stability on the server, by restricting each individual cPanel account to a set of resource limits (similar to how a VPS works)

There is an article here which explains how LVE limits work (and their benefits etc)…

[What are CloudLinux and LVE limits?](https://srvplex.com/clients/knowledgebase/8/What-are-CloudLinux-and-LVE-limits.html)

So, each individual account has:

1 CPU, 1GB RAM and 30 Entry Processes (unless otherwise stated on your package)

Only if the account hits it LVE limits would it display a 503 error (which is called a ‘fault’).

The above limits are fairly high for a shared account, however can be scaled if required, by upgrading an account to the Elastic Cloud service.

However, as mentioned those limits are fairly high for a standard PHP application. As such, I would first recommend reviewing the plugins the site uses and check for any abnormal traffic etc.

A few things to check…

    Disable any unnecessary plugins (or ideally, temporarily disable them all, leave it a couple of minutes then re-enable to see if that’s the issue)
    Try to enable caching in the site (we would definitely recommend Bolt-Cache! This was developed in-house, and is available from cPanel -> Bolt-Cache) as this may reduce the resource usage dramatically
    Cron Jobs that run heavy processes too frequently
    Backup ‘plugins’ or backup scripts. These can definitely cause an account to max out it’s LVE limits, due to the disk / memory intensive processes required to generate the backups
    Processes that consume large MySQL throughout

These are just a few, but it’s also possible the account is under some kind of attack etc

You can check your LVE usage via the cPanel -> CPU and concurrent connections screen in cPanel.

You can read more about LVE limits here…

What are CloudLinux and LVE limits?

By analysing the Snapshots as described in the document, you will be able to see the processes running at the time of hitting your limit.

If the above doesn’t help though, of course, we would be more than happy to investigate this further for you.

If you could spend a little time reviewing the above, then still can’t resolve the issue then do get in touch.

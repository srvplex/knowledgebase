
#Restore your site with R1Soft

Find the R1Soft icon.
https://i1.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/rs1soft_icon.png?resize=510%2C183&ssl=1

Find the date you want to restore too.
https://i2.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/recovery_points.png?resize=980%2C313&ssl=1

Click this icon to restore a file or folder.
https://i1.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/recovery-files.png?resize=96%2C320&ssl=1

This is the icon for a database.
https://i2.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/recovery_database.png?resize=76%2C337&ssl=1

You can restore the entire home directory or double-click home to browse to a specific file or folder.
https://i1.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/recovery_home.png?resize=818%2C107&ssl=1

Select the file or folders and click Restore selected.
https://i0.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/select_folder-and-click-restore.png?resize=861%2C275&ssl=1 and click restore

Click restore on the warning.
https://i0.wp.com/www.pickaweb.co.uk/kb/wp-content/uploads/2014/09/restore_warning.png?resize=475%2C156&ssl=1

And the files will be restored.

That’s how you can restore your website easily using R1soft.

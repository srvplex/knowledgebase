#How to download or access your raw access logs in cPanel (access_log)

Raw Access Logs allow you to see who has visited your website without displaying graphs, charts, or other graphics. You can use the Raw Access Logs menu to download a zipped version of the server’s access log for your site. This can be very useful when you want to quickly see who has visited your site.

 As SRVPlex cPanel hosting has two types of configuration (native or ‘default’ which uses mod_lsapi, or ‘pure nginx’ using the CLOUDNS / Bolt-Cache plugins) the access logs will be located in the following…

##Native Configuration (lsapi):

`/home/accountname/access-logs` You can also access your raw access logs by going to ‘Metrics -> Raw Access’ from within cPanel.

##Pure Nginx / PHP-FPM / CloudNS and Bolt-Cache:

`/home/accountname/logs`

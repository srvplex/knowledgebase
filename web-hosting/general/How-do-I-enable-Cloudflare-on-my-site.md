
#How do I enable Cloudflare on my site?

##Making the Internet Work the Way It Should

Cloudflare speeds up and protects millions of websites, APIs, SaaS services, and other properties connected to the Internet. Their Anycast technology enables our benefits to scale with every server we add to our growing footprint of data centers.

###PERFORMANCE

Cloudflare dramatically improves website performance through our global [CDN](https://www.cloudflare.com/cdn/) and web optimization features.

###SECURITY

Cloudflare’s [WAF](https://www.cloudflare.com/waf/), [DDoS protection](https://www.cloudflare.com/ddos/), and SSL defend website owners and their visitors from all types of online threats.

###RELIABILITY

With over 35% market share, Cloudflare runs the largest, fastest, and most reliable [managed DNS](https://www.cloudflare.com/dns/) service in the world.

###INSIGHT

Cloudflare’s network helps identify visitor and bot behavior that isn’t accessible to conventional analytics technologies.

##Enabling CloudFlare

To enable Cloudflare, please do the following…

    1. Login to cPanel
    2. Go to ‘Software -> Cloudflare’
    3. Login
    4. Click ‘Enable Cloudflare on this Domain’
